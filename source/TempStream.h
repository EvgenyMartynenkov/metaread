//---------------------------------------------------------------------------

#ifndef TempStreamH
#define TempStreamH

#include <System.Classes.hpp>

class TTempStreamStaticInit
{
public:
	__fastcall TTempStreamStaticInit();
	__fastcall ~TTempStreamStaticInit();
};


//---------------------------------------------------------------------------
class TTempStream : public THandleStream
{
public:
	static String tempcat;
	static String tempname;
	static long tempno;

	__fastcall TTempStream();
	__fastcall ~TTempStream();
};

//---------------------------------------------------------------------------
#endif
